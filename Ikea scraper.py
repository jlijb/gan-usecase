# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 15:35:26 2019

@author: JLijbers
"""

from bs4 import BeautifulSoup
import requests
import os

cwd = os.getcwd()

#Site to download from
url = 'https://www.ikea.com/nl/nl/catalog/categories/departments/dining/25219/'

#fetch content
url_response = requests.get(url, timeout=5)

#parse html
url_content = BeautifulSoup(url_response.content, "html.parser")

#go to products
products = url_content.find(id='productLists')

#extract all html elements where an image is stored
images = products.find_all('img', {"class": "prodImg"})

#extract and download images
count = 0
for image in images:
    count += 1
    name = 'ikea_' + str(count)
    image_src = 'https://www.ikea.com/' + image['src']
    response = requests.get(image_src).content
    filename = cwd + "/" + name + ".jpg"
    with open(filename, 'wb') as f:
        f.write(response)
