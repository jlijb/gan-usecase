# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 12:54:07 2019

@author: JLijbers
"""
from bs4 import BeautifulSoup
import requests
import numpy as np

#Set path - TO DO: change this to automatically be the path of script-location
path = "C://Users//Jlijbers//Documents//AA//AI//GAN//Ontwerp use-case"

#Site to download from
urlsource = "https://www.woonboulevardpoortvliet.nl/wonen/stoelen/eetkamerstoelen?limit=48"
pages = np.arange(13)+1

#Go over all pages
count = 0
for page in pages:
    
    #Set specific page url
    url = urlsource + "&p=" + str(page)

    #fetch content
    url_response = requests.get(url, timeout=45)
    
    #parse html
    url_content = BeautifulSoup(url_response.content, "html.parser")
    
    #go to products
    products = url_content.find('div', {"class": "category-products"})
    
    #extract all html elements where an image is stored
    images = products.find_all('img', {"width": "229"})
    
    #extract and download images
    for image in images:
        count += 1
        name = 'poortvliet_' + str(count)
        image_src = image['src']
        response = requests.get(image_src).content
        filename = path + "/" + name + ".jpg"
        with open(filename, 'wb') as f:
            f.write(response)
