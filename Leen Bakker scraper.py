# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 12:07:22 2019

@author: JLijbers
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 15:35:26 2019

@author: JLijbers
"""

from bs4 import BeautifulSoup
import requests
import os

cwd = os.getcwd()

#Site to download from
url = 'https://www.leenbakker.nl/banken-en-stoelen/stoelen/eetkamerstoelen?fromPage=catalogEntryList&beginIndex=0&pageSize=467&pageView=grid'

#fetch content
url_response = requests.get(url, timeout=30)

#parse html
url_content = BeautifulSoup(url_response.content, "html.parser")

#go to products
products = url_content.find(id='plp_wrapper')

#extract all html elements where an image is stored
images = products.find_all('img', {"onerror": "this.onerror=null;this.src='/wcsstore/LBStorefrontAssetStore/images/NoImage_300x300.png';"})

#extract and download images
count = 0
for image in images:
    count += 1
    name = 'leenbakker_' + str(count)
    image_src = 'https:' + image['src']
    response = requests.get(image_src).content
    filename = cwd + "/" + name + ".jpg"
    with open(filename, 'wb') as f:
        f.write(response)
