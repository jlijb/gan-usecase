# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 12:54:07 2019

@author: JLijbers
"""
from bs4 import BeautifulSoup
import requests
import numpy as np

#Set path - TO DO: change this to automatically be the path of script-location
path = "C://Users//Jlijbers//Documents//AA//AI//GAN//Ontwerp use-case"

#Site to download from
urlsource = "https://www.eijerkamp.nl/eetkamer/eetkamerstoelen/"
pages = np.arange(11)+1

#Go over all pages
count = 0
for page in pages:
    
    #Set specific page url
    url = urlsource + str(page) + "/_IC80/"

    #fetch content
    url_response = requests.get(url, timeout=45)
    
    #parse html
    url_content = BeautifulSoup(url_response.content, "html.parser")
    
    #go to products
    products = url_content.find_all('div', {"class" : "clearfix products grid-l-3-van-4 isotope"})
          
    #extract all html elements where an image is stored
    images = url_content.find_all('img', {"class": "position-center"})
       
    #extract and download images
    for image in images:
        count += 1
        name = 'eijerkamp_' + str(count)
        image_src = image['data-src']
        try:
            response = requests.get(image_src).content
            filename = path + "/" + name + ".jpg"
            with open(filename, 'wb') as f:
                f.write(response)
        except:
            pass
